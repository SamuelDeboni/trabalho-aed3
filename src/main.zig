const std = @import("std");
const warn = std.debug.warn;
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;

const db = @import("database.zig");
const entity = @import("entity.zig");
const Entity = entity.Entity;
const Suggestion = entity.Suggestion;
const Group = entity.Group;
const Invite = entity.Invite;

const al = std.heap.page_allocator;

// ===== String =====
const StrCompareResult = struct {
    eq: bool,
    eq_until: i32,
    greaterness: i32,
};

fn strCompare(a: []const u8, b: []const u8) StrCompareResult {
    var result = StrCompareResult{
        .eq = true,
        .eq_until = -1,
        .greaterness = 0,
    };
    for (a) |c, i| {
        if (i < b.len) {
            if (c == b[i]) {
                result.eq_until = @intCast(i32, i);
            } else {
                result.greaterness = @intCast(i32, c) - @intCast(i32, b[i]);
                result.eq = false;
                break;
            }
        } else {
            result.eq = false;
            break;
        }
    }
    if (a.len != b.len) {
        result.eq = false;
    }
    return result;
}
// =================

fn clean() !void {
    const stdout = std.io.getStdOut().outStream();
    var i: u32 = 0;
    while (i < 120) : (i += 1) {
        try stdout.print("\n", .{});
    }
}

fn readLine(buffer: []u8) !u32 {
    const stdin = std.io.getStdIn().inStream();
    var result: u32 = 0;
    const b = (try stdin.readUntilDelimiterOrEof(buffer[0..], '\n'));
    if (b) |b1| {
        result = @intCast(u32, b1.len);
    }
    return result;
}

fn readFloat(comptime T: type, stdin: var) !T {
    var buffer = [_]u8{0} ** 256;
    const n_str_len = try readLine(buffer[0..]);
    var n: T = try std.fmt.parseFloat(T, buffer[0..n_str_len]);
    return n;
}

fn readInt(comptime T: type, stdin: var) !T {
    var buffer = [_]u8{0} ** 256;
    const n_str_len = try readLine(buffer[0..]);
    var n: T = try std.fmt.parseInt(T, buffer[0..n_str_len], 10);
    return n;
}

fn readIntUntilCorrect(comptime fmt: []const u8, args: var, comptime T: type) T {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var result: T = 0;
    while (true) {
        clean() catch {};
        stdout.print(fmt, args) catch {};
        result = readInt(T, stdin) catch continue;
        break;
    }
    return result;
}

fn manageGroups(user_id: u32) !void {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var option: u8 = 0;
    var buffer: [255]u8 = [_]u8{0} ** 255;

    while (true) {
        try clean();
        try stdout.print(
            \\  AMIGO OCULTO 
            \\================
            \\INÍCIO > GRUPOS > GERENCIAMENTO DE GRUPOS > GRUPOS
            \\
            \\1) Listar
            \\2) Incluir
            \\3) Alterar
            \\4) Desativar
            \\
            \\0) Retornar
            \\
            \\> 
        , .{});
        option = try stdin.readIntLittle(u8);
        _ = try readLine(buffer[0..]);

        switch (option) {
            '0' => break,
            '1' => {
                const ids = try db.listSuggestions(al, group_ctx, user_id);
                defer al.free(ids);
                for (ids) |id, i| {
                    var tmp = try db.read(Group, al, group_ctx, id);
                    try stdout.print("\n{})\t{}\n\t{}\n\t{}\n\tR$ {d:.2}\n\t{}\n\t{}\n\t{}\n\t{}\n", .{
                        (i + 1),
                        tmp.name,
                        tmp.draw_date,
                        tmp.meeting_date,
                        @intToFloat(f32, tmp.price) / 100,
                        tmp.is_sorted,
                        tmp.is_active,
                        tmp.meeting_place,
                        tmp.obs,
                    });
                    tmp.free(al);
                }
                _ = try readLine(buffer[0..]);
            },
            '2' => {
                try clean();
                try stdout.print(
                    \\Digite o nome do grupo:
                    \\
                    \\> 
                , .{});
                var name_len = try readLine(buffer[0..]);
                assert(name_len < 254);
                var name: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, name[0..], buffer[0..name_len]);

                var draw_date = readIntUntilCorrect(
                    \\Digite a data do sorteio:
                    \\
                    \\>
                , .{}, u64);

                var meeting_date = readIntUntilCorrect(
                    \\Digite a data do encontro:
                    \\
                    \\> 
                , .{}, u64);

                var price = readIntUntilCorrect(
                    \\Digite o preço da sugerido para o grupo:
                    \\
                    \\> 
                , .{}, i32);

                try clean();
                try stdout.print(
                    \\Digite o lugar de encontro:
                    \\
                    \\> 
                , .{});
                var meeting_place_len = try readLine(buffer[0..]);
                assert(meeting_place_len < 254);
                var meeting_place: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, meeting_place[0..], buffer[0..meeting_place_len]);

                try clean();
                try stdout.print(
                    \\Observacao:
                    \\
                    \\> 
                , .{});
                var obs_len = try readLine(buffer[0..]);
                assert(obs_len < 254);
                var obs: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, obs[0..], buffer[0..obs_len]);

                var new_group = Group{
                    .id = 0,
                    .user_id = user_id,
                    .draw_date = draw_date,
                    .meeting_date = meeting_date,
                    .price = price,

                    .is_sorted = @boolToInt(false),
                    .is_active = @boolToInt(false),

                    .name = name[0..name_len],
                    .meeting_place = meeting_place[0..meeting_place_len],
                    .obs = obs[0..obs_len],
                };
                const new_group_data = try new_group.toByteArray(al);
                try db.createSuggestion(al, &group_ctx, new_group_data, user_id);
            },
            '3' => {
                const ids = try db.listSuggestions(al, group_ctx, user_id);
                defer al.free(ids);

                if (ids.len >= 1) {
                    var group_number: usize = 0;
                    while (true) {
                        try clean();
                        try stdout.print(
                            \\Digite o grupo a ser atualizado [1 ate {}]:
                            \\
                            \\> 
                        , .{ids.len});
                        group_number = readInt(usize, stdin) catch continue;
                        if (group_number > 0 and group_number <= ids.len) break;
                    }

                    try clean();
                    try stdout.print(
                        \\Digite o nome do grupo:
                        \\
                        \\> 
                    , .{});
                    var name_len = try readLine(buffer[0..]);
                    assert(name_len < 254);
                    var name: [254]u8 = [_]u8{0} ** 254;
                    std.mem.copy(u8, name[0..], buffer[0..name_len]);

                    var draw_date = readIntUntilCorrect(
                        \\Digite a data do sorteio:
                        \\
                        \\> 
                    , .{}, u64);

                    var meeting_date = readIntUntilCorrect(
                        \\Digite a data do encontro:
                        \\
                        \\> 
                    , .{}, u64);

                    var price = readIntUntilCorrect(
                        \\Digite o preço da sugerido para o grupo:
                        \\
                        \\> 
                    , .{}, i32);

                    try clean();
                    try stdout.print(
                        \\Digite o lugar de encontro:
                        \\
                        \\> 
                    , .{});

                    var meeting_place_len = try readLine(buffer[0..]);
                    assert(meeting_place_len < 254);
                    var meeting_place: [254]u8 = [_]u8{0} ** 254;
                    std.mem.copy(u8, meeting_place[0..], buffer[0..meeting_place_len]);

                    try clean();
                    try stdout.print(
                        \\Observacao:
                        \\
                        \\> 
                    , .{});
                    var obs_len = try readLine(buffer[0..]);
                    assert(obs_len < 254);
                    var obs: [254]u8 = [_]u8{0} ** 254;
                    std.mem.copy(u8, obs[0..], buffer[0..obs_len]);

                    var new_group = Group{
                        .id = ids[group_number - 1],
                        .user_id = user_id,
                        .draw_date = draw_date,
                        .meeting_date = meeting_date,
                        .price = price,

                        .is_sorted = @boolToInt(false),
                        .is_active = @boolToInt(false),

                        .name = name[0..name_len],
                        .meeting_place = meeting_place[0..meeting_place_len],
                        .obs = obs[0..obs_len],
                    };
                    const new_group_data = try new_group.toByteArray(al);
                    try db.update(al, &group_ctx, new_group);
                } else {
                    try clean();
                    try stdout.print(
                        \\Nao ha grupos para esse usuario
                    , .{});
                    _ = try readLine(buffer[0..]);
                }
            },
            '4' => {
                const ids = try db.listSuggestions(al, group_ctx, user_id);
                defer al.free(ids);

                if (ids.len >= 1) {
                    try clean();
                    try stdout.print(
                        \\Digite o grupo a ser deletado [1 ate {}]:
                        \\
                        \\> 
                    , .{ids.len});
                    var group_number: u8 = try stdin.readIntLittle(u8);
                    group_number = group_number - '0';
                    assert(group_number >= 1 and group_number <= ids.len);

                    try db.deleteSuggestion(
                        @intCast(u32, ids[group_number - 1]),
                        group_ctx,
                    );
                } else {
                    try clean();
                    try stdout.print(
                        \\Nao ha grupos para esse usuario
                    , .{});
                    _ = try readLine(buffer[0..]);
                }
            },
            else => {},
        }
    }
}

fn manageInvites(user_id: u32) !void {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var option: u8 = 0;
    var buffer: [255]u8 = [_]u8{0} ** 255;

    while (true) {
        try clean();
        try stdout.print(
            \\  AMIGO OCULTO 
            \\================
            \\
            \\INÍCIO > GRUPOS > GERENCIAMENTO DE GRUPOS > CONVITES
            \\
            \\1) Listagem dos convites
            \\2) Emissão de convites
            \\3) Cancelamento de convites
            \\
            \\0) Retornar ao menu anterior
            \\
            \\> 
        , .{});
        option = try stdin.readIntLittle(u8);
        _ = try readLine(buffer[0..]);

        switch (option) {
            '1' => {
                const group_ids = try db.listSuggestions(al, group_ctx, user_id);
                defer al.free(group_ids);

                // Continua o loop caso não tiver grupos
                if (group_ids.len == 0) continue;

                // Lista os grupos
                for (group_ids) |id, i| {
                    var tmp = try db.read(Group, al, group_ctx, id);
                    try stdout.print("{}\t{}\n", .{
                        (i + 1),
                        tmp.name,
                    });
                    tmp.free(al);
                }

                var group_number: usize = 0;
                while (true) {
                    try clean();
                    try stdout.print(
                        \\Escolha um grupo [1 ate {}]:
                        \\
                        \\> 
                    , .{group_ids.len});
                    group_number = readInt(usize, stdin) catch continue;
                    if (group_number >= 0 and group_number <= group_ids.len) break;
                }
                if (group_number == 0) continue;
                group_number -= 1;

                const invite_ids = try db.listSuggestions(al, invite_ctx, group_ids[group_number]);
                if (invite_ids.len == 0) continue;

                for (invite_ids) |id, i| {
                    var tmp = try db.read(Invite, al, invite_ctx, id);
                    try stdout.print(
                        "{}. {} ({} - {})\n",
                        .{
                            i + 1,
                            tmp.email,
                            tmp.invite_date,
                            tmp.state,
                        },
                    );
                }

                _ = try readLine(buffer[0..]);
            },
            '2' => {
                const group_ids = try db.listSuggestions(al, group_ctx, user_id);
                defer al.free(group_ids);

                // Continua o loop caso não tiver grupos
                if (group_ids.len == 0) continue;

                // Lista os grupos
                for (group_ids) |id, i| {
                    var tmp = try db.read(Group, al, group_ctx, id);
                    try stdout.print("{}\t{}\n", .{
                        (i + 1),
                        tmp.name,
                    });
                    tmp.free(al);
                }

                var group_number: usize = 0;
                while (true) {
                    try clean();
                    try stdout.print(
                        \\Escolha um grupo [1 ate {}]:
                        \\
                        \\> 
                    , .{group_ids.len});
                    group_number = readInt(usize, stdin) catch continue;
                    if (group_number >= 0 and group_number <= group_ids.len) break;
                }
                if (group_number == 0) continue;
                group_number -= 1;

                try clean();
                try stdout.print(
                    \\Digite o email para o novo convite:
                    \\
                    \\> 
                , .{});

                var email_len = try readLine(buffer[0..]);
                assert(email_len < 254);
                var email: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, email[0..], buffer[0..email_len]);

                const new_invite = Invite{
                    .id = 0,
                    .group_id = group_ids[group_number],
                    .invite_date = 42, // TODO
                    .state = 0,
                    .email = email[0..email_len],
                };

                var new_invite_data = try new_invite.toByteArray(al);
                try db.createSuggestion(
                    al,
                    &invite_ctx,
                    new_invite_data,
                    new_invite.group_id,
                );
                al.free(new_invite_data);
            },
            '0' => break,
            else => {},
        }
    }
}

fn groupsLoop(user_id: u32) !void {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var option: u8 = 0;
    var buffer: [255]u8 = [_]u8{0} ** 255;

    while (true) {
        try clean();
        try stdout.print(
            \\  AMIGO OCULTO 
            \\================
            \\
            \\INÍCIO > GRUPOS
            \\
            \\1) Criação e gerenciamento de grupos
            \\2) Participação nos grupos
            \\
            \\0) Retornar
            \\
            \\> 
        , .{});
        option = try stdin.readIntLittle(u8);
        _ = try readLine(buffer[0..]);

        switch (option) {
            '0' => break,
            '1' => {
                while (true) {
                    try clean();
                    try stdout.print(
                        \\  AMIGO OCULTO 
                        \\================
                        \\INÍCIO > GRUPOS > GERENCIAMENTO DE GRUPOS
                        \\
                        \\1) Grupos
                        \\2) Convites
                        \\3) Participantes
                        \\4) Sorteio
                        \\
                        \\0) Retornar
                        \\
                        \\> 
                    , .{});
                    option = try stdin.readIntLittle(u8);
                    _ = try readLine(buffer[0..]);

                    switch (option) {
                        '0' => break,
                        '1' => try manageGroups(user_id),
                        '2' => try manageInvites(user_id),
                        else => {},
                    }
                }
            },
            else => {},
        }
    }
}

fn suggestionLoop(user_id: u32) !void {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var option: u8 = 0;
    var buffer: [255]u8 = [_]u8{0} ** 255;

    while (true) {
        try clean();
        try stdout.print(
            \\  AMIGO OCULTO 
            \\================
            \\
            \\INÍCIO > SUGESTÕES
            \\
            \\1) Listar
            \\2) Incluir
            \\3) Alterar
            \\4) Excluir
            \\
            \\0) Retornar
            \\> 
        , .{});

        option = try stdin.readIntLittle(u8);
        _ = try readLine(buffer[0..]);

        switch (option) {
            '0' => break,
            '1' => {
                const ids = try db.listSuggestions(al, sugg_ctx, user_id);
                defer al.free(ids);

                for (ids) |id, i| {
                    var tmp = try db.read(Suggestion, al, sugg_ctx, id);
                    try stdout.print("\n{})\t{}\n\t{}\n\tR$ {d:.2}\n\t{}\n", .{
                        (i + 1),
                        tmp.product,
                        tmp.store,
                        @intToFloat(f32, tmp.price) / 100.0,
                        tmp.obs,
                    });
                    tmp.free(al);
                }
                _ = try readLine(buffer[0..]);
            },
            '2' => {
                var price: f32 = 0;
                while (true) {
                    try clean();
                    try stdout.print(
                        \\Digite o preço da sugestao:
                        \\
                        \\> 
                    , .{});

                    price = readFloat(f32, stdin) catch continue;

                    break;
                }

                try clean();
                try stdout.print(
                    \\Digite o produto da sugestao:
                    \\
                    \\> 
                , .{});
                var product_len = try readLine(buffer[0..]);
                assert(product_len < 254);
                var product: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, product[0..product_len], buffer[0..product_len]);

                try clean();
                try stdout.print(
                    \\Digite onde e possivel encontrar o produto da sugestao:
                    \\
                    \\> 
                , .{});
                var store_len = try readLine(buffer[0..]);
                assert(store_len < 254);
                var store: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, store[0..store_len], buffer[0..store_len]);

                try clean();
                try stdout.print(
                    \\Observacao:
                    \\
                    \\> 
                , .{});
                var obs_len = try readLine(buffer[0..]);
                assert(obs_len < 254);
                var obs: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, obs[0..obs_len], obs[0..obs_len]);

                var new_suggestion = Suggestion{
                    .id = 0,
                    .user_id = user_id,
                    .price = @floatToInt(i32, price * 100),
                    .product = product[0..],
                    .store = store[0..],
                    .obs = obs[0..],
                };
                const new_sugg_data = try new_suggestion.toByteArray(al);
                try db.createSuggestion(al, &sugg_ctx, new_sugg_data, user_id);
            },
            '3' => {
                const ids = try db.listSuggestions(al, sugg_ctx, user_id);
                defer al.free(ids);

                if (ids.len >= 1) {
                    var suggestion_number: usize = 0;
                    while (true) {
                        try clean();
                        try stdout.print(
                            \\Digite a sugestao a ser atualizada [1 ate {}]:
                            \\
                            \\> 
                        , .{ids.len});
                        suggestion_number = readInt(usize, stdin) catch continue;
                        if (suggestion_number > 0 and suggestion_number <= ids.len) break;
                    }

                    var price: f32 = 0;
                    while (true) {
                        try clean();
                        try stdout.print(
                            \\Digite o novo preço da sugestao:
                            \\
                            \\> 
                        , .{});
                        price = readFloat(f32, stdin) catch continue;
                        break;
                    }

                    try clean();
                    try stdout.print(
                        \\Digite o novo produto da sugestao:
                        \\
                        \\> 
                    , .{});
                    var product_len = try readLine(buffer[0..]);
                    assert(product_len < 254);
                    var product: [254]u8 = [_]u8{0} ** 254;
                    std.mem.copy(u8, product[0..product_len], buffer[0..product_len]);

                    try clean();
                    try stdout.print(
                        \\Digite onde e possivel encontrar o produto da sugestao:
                        \\
                        \\> 
                    , .{});
                    var store_len = try readLine(buffer[0..]);
                    assert(store_len < 254);
                    var store: [254]u8 = [_]u8{0} ** 254;
                    std.mem.copy(u8, store[0..store_len], buffer[0..store_len]);

                    try clean();
                    try stdout.print(
                        \\Nova observacao:
                        \\
                        \\> 
                    , .{});
                    var obs_len = try readLine(buffer[0..]);
                    assert(obs_len < 254);
                    var obs: [254]u8 = [_]u8{0} ** 254;
                    std.mem.copy(u8, obs[0..obs_len], obs[0..obs_len]);

                    var new_suggestion = Suggestion{
                        .id = ids[suggestion_number - 1],
                        .user_id = user_id,
                        .price = @floatToInt(i32, price * 100),
                        .product = product[0..],
                        .store = store[0..],
                        .obs = obs[0..],
                    };
                    const new_sugg_data = try new_suggestion.toByteArray(al);
                    try db.update(al, &sugg_ctx, new_suggestion);
                    //update suggestion
                } else {
                    try clean();
                    try stdout.print(
                        \\Nao ha sugestoes para esse usuario
                    , .{});
                    _ = try readLine(buffer[0..]);
                }
            },
            '4' => {
                const ids = try db.listSuggestions(al, sugg_ctx, user_id);
                defer al.free(ids);

                if (ids.len >= 1) {
                    try clean();
                    try stdout.print(
                        \\Digite a sugestao a ser deletada [1 ate {}]:
                        \\
                        \\> 
                    , .{ids.len});
                    var suggestion_number: u8 = try stdin.readIntLittle(u8);
                    suggestion_number = suggestion_number - '0';
                    assert(suggestion_number >= 1 and suggestion_number <= ids.len);

                    try db.deleteSuggestion(
                        @intCast(u32, ids[suggestion_number - 1]),
                        sugg_ctx,
                    );
                } else {
                    try clean();
                    try stdout.print(
                        \\Nao ha sugestoes para esse usuario
                    , .{});
                    _ = try readLine(buffer[0..]);
                }
            },
            else => {},
        }
    }
}

fn mainLoop(user_id: u32) !void {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var option: u8 = 0;
    var buffer: [255]u8 = [_]u8{0} ** 255;

    while (true) {
        var invites_ids: [4096]u32 = [_]u32{0} ** 4096;
        var invites_qnt: usize = 0;

        const user = try db.read(Entity, al, user_ctx, user_id);
        defer user.free(al);
        const user_email = user.email;
        var i: u32 = 1;
        while (i <= invite_ctx.last_id) : (i += 1) {
            const it = try db.read(Invite, al, invite_ctx, i);
            if (strCompare(it.email, user_email).eq and it.state == 0) {
                invites_ids[invites_qnt] = i;
                invites_qnt += 1;
            }
        }

        try clean();
        try stdout.print(
            \\==== Amigo Oculto ====
            \\INICIO
            \\
            \\1) Sugestões de presentes
            \\2) Grupos
            \\3) Novos convites: {}
            \\
            \\0) Retornar
            \\
            \\> 
        , .{invites_qnt});

        option = try stdin.readIntLittle(u8);
        _ = try readLine(buffer[0..]);

        switch (option) {
            '0' => break,
            '1' => try suggestionLoop(user_id),
            '2' => try groupsLoop(user_id),
            '3' => {
                {
                    while (true) {
                        try clean();

                        var j: u32 = 0;
                        while (j < invites_qnt) : (j += 1) {
                            const it = try db.read(Invite, al, invite_ctx, invites_ids[j]);
                            if (it.state == 0) {
                                const group = try db.read(Group, al, group_ctx, it.group_id);
                                try stdout.print(
                                    \\{})  
                                    \\    {}
                                    \\    Convidado em {}
                                    \\    Por {}
                                    \\
                                , .{
                                    (j + 1),
                                    group.name,
                                    it.invite_date,
                                    user.name,
                                });
                            }
                        }

                        if (invites_qnt > 0) {
                            try stdout.print(
                                \\
                                \\Escolha um convite [1 ate {}]:
                                \\> 
                            , .{invites_qnt});
                            const invite_to_edit_id = try readInt(u32, stdin);
                            try stdout.print(
                                \\
                                \\A para aceitar, R para recusar, V para voltar
                                \\> 
                            , .{});
                            switch (try stdin.readIntLittle(u8)) {
                                'A' => {
                                    var new_invite = try db.read(Invite, al, invite_ctx, invite_to_edit_id);
                                    new_invite.state = 1;
                                    try db.update(al, &invite_ctx, new_invite);
                                    _ = try stdin.readIntLittle(u8);
                                },
                                'R' => {
                                    var new_invite = try db.read(Invite, al, invite_ctx, invite_to_edit_id);
                                    new_invite.state = 2;
                                    try db.update(al, &invite_ctx, new_invite);
                                    _ = try stdin.readIntLittle(u8);
                                },
                                'V' => break,
                                else => {
                                    try stdout.print(
                                        \\
                                        \\Opcao invalida
                                        \\
                                    , .{});
                                    _ = try stdin.readIntLittle(u8);
                                    _ = try stdin.readIntLittle(u8);
                                },
                            }
                        } else {
                            break;
                        }
                    }
                }
            },
            else => {},
        }
    }
}

var user_ctx: db.Ctx = undefined;
var sugg_ctx: db.Ctx = undefined;
var group_ctx: db.Ctx = undefined;
var invite_ctx: db.Ctx = undefined;

pub fn main() anyerror!void {
    const stdin = std.io.getStdIn().inStream();
    const stdout = std.io.getStdOut().outStream();
    var option: u8 = 0;
    var buffer: [255]u8 = [_]u8{0} ** 255;

    user_ctx = try db.open("data.db", false);
    defer (db.close(user_ctx));

    sugg_ctx = try db.open("suggestion.db", false);
    defer (db.close(sugg_ctx));

    group_ctx = try db.open("group.db", false);
    defer (db.close(group_ctx));

    invite_ctx = try db.open("invite.db", false);
    defer (db.close(invite_ctx));

    while (true) {
        try clean();
        try stdout.print(
            \\==== Amigo Oculto ====
            \\ACESSO
            \\
            \\1) Acesso ao sistema
            \\2) Novo usuário (primeiro acesso)
            \\
            \\0) Sair
            \\
            \\> 
        , .{});

        option = try stdin.readIntLittle(u8);
        _ = try readLine(buffer[0..]);
        switch (option) {
            '0' => break,
            '1' => read_blk: {
                try clean();
                try stdout.print(
                    \\Digite o email de acesso:
                    \\
                    \\> 
                , .{});

                var id: u32 = 0;
                while (true) {
                    const email_size = try readLine(buffer[0..]);
                    std.debug.warn("{}\n", .{email_size});

                    if (email_size == 0) break :read_blk;

                    id = db.readSecKey(
                        user_ctx,
                        buffer[0..email_size],
                    ) catch |err| switch (err) {
                        db.CrudError.KeyNotFound => {
                            try stdout.print(
                                \\Email de acesso invalido,
                                \\Digite o email novamente:
                                \\Deixe vazio para voltar
                                \\
                                \\> 
                            , .{});
                            continue;
                        },
                        else => {
                            unreachable;
                        },
                    };
                    break;
                }

                var tmp = try db.read(Entity, al, user_ctx, id);
                defer tmp.free(al);

                try clean();
                try stdout.print(
                    \\Digite a senha de acesso:
                    \\
                    \\>
                , .{});

                const password_size = try readLine(buffer[0..]);
                std.debug.warn("{}\n{}\n", .{ buffer[0..password_size], tmp.password });
                if (strCompare(buffer[0..password_size], tmp.password).eq) {
                    try mainLoop(tmp.id);
                }
            },
            '2' => {
                //get the new user name
                try clean();
                try stdout.print(
                    \\Digite o nome do novo usuário:
                    \\
                    \\> 
                , .{});
                //_ = try stdin.readIntLittle(u8);
                const name_size = try readLine(buffer[0..]);
                assert(name_size < 254);

                var name: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, name[0..name_size], buffer[0..name_size]);

                //get the new user email
                try clean();
                try stdout.print(
                    \\Digite o email do novo usuário:
                    \\
                    \\> 
                , .{});
                const email_size = try readLine(buffer[0..]);
                assert(email_size < 254);
                var email: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, email[0..email_size], buffer[0..email_size]);

                //get the new user password
                try clean();
                try stdout.print(
                    \\Digite a senha do novo usuário:
                    \\
                    \\> 
                , .{});
                const password_size = try readLine(buffer[0..]);
                assert(password_size < 254);
                var password: [254]u8 = [_]u8{0} ** 254;
                std.mem.copy(u8, password[0..password_size], buffer[0..password_size]);

                //put the new user in database
                var new_user = Entity{
                    .id = 0,
                    .name = name[0..name_size],
                    .email = email[0..email_size],
                    .password = password[0..password_size],
                };
                const new_user_data = try new_user.toByteArray(al);
                try db.create(al, &user_ctx, new_user_data, new_user.email);

                var new_suggestion = Suggestion{
                    .id = 0,
                    .user_id = 0,
                    .price = 10.00,
                    .product = "Batatinha",
                    .store = "Potato shop",
                    .obs = "yay",
                };
                const new_sugg_data = try new_suggestion.toByteArray(al);
                try db.createSuggestion(al, &sugg_ctx, new_sugg_data, 3);
            },
            else => {
                break;
            },
        }
    }
}
