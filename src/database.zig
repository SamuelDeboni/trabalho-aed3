const std = @import("std");
const Allocator = std.mem.Allocator;
const File = std.fs.File;
const toBytes = std.mem.toBytes;
const entity = @import("entity.zig");
const Entity = entity.Entity;
const Suggestion = entity.Suggestion;
const Group = entity.Group;
const Invite = entity.Invite;

var db_file: File = undefined;
var di_file: File = undefined;
var ii_file: File = undefined;

pub const Ctx = struct {
    db_file: File,
    di_file: File,
    ii_file: File,

    length: u64 = 4,
    last_id: u32 = 0,
    last_pos: u64 = @sizeOf(u64) * 2 + @sizeOf(u32),
};

pub const CrudError = error{
    InvalidIndex,
    KeyNotFound,
    EmailToBig,
};

/// Read a int from a file on @pos
fn readIntFromFile(comptime T: type, file: File, pos: u64) !T {
    try file.seekTo(pos);
    var buf = [_]u8{0} ** @divExact(T.bit_count, 8);
    _ = try file.readAll(buf[0..]);
    return std.mem.readIntLittle(T, buf[0..]);
}

/// write a int @n to a file on @pos
fn writeIntToFile(comptime T: type, file: File, n: T, pos: u64) !void {
    try file.seekTo(pos);
    try file.writeAll(toBytes(n)[0..]);
}

/// Open a database
pub fn open(comptime db_name: []const u8, overide_files: bool) !Ctx {
    var ctx: Ctx = undefined;

    ctx.length = 4;
    ctx.last_id = 0;
    ctx.last_pos = 0x14;
    const current_path = std.fs.cwd();

    var new_header = false;

    if (overide_files) {
        current_path.deleteFile(db_name) catch {};
        current_path.deleteFile("di_" ++ db_name) catch {};
        current_path.deleteFile("ii_" ++ db_name) catch {};
    }

    // Try to open the file, in case of file dont exist,
    // create a new one
    ctx.db_file =
        current_path.openFile(
        db_name,
        .{ .read = true, .write = true },
    ) catch |err| blk: {
        if (err == File.OpenError.FileNotFound) {
            new_header = true;
            break :blk try current_path.createFile(
                db_name,
                .{ .read = true, .truncate = false },
            );
        } else {
            std.debug.panic("Error {}\n", .{err});
        }
    };
    errdefer ctx.db_file.close();

    // Open index
    ctx.di_file = try current_path.createFile(
        "di_" ++ db_name,
        .{ .read = true, .truncate = false },
    );
    errdefer ctx.di_file.close();

    ctx.ii_file = try current_path.createFile(
        "ii_" ++ db_name,
        .{ .read = true, .truncate = false },
    );
    errdefer ctx.ii_file.close();

    if (new_header) {
        try ctx.db_file.writeAll(toBytes(ctx.length)[0..]);
        try ctx.db_file.writeAll(toBytes(ctx.last_id)[0..]);
        try ctx.db_file.writeAll(toBytes(ctx.last_pos)[0..]);
    } else {
        ctx.length = try readIntFromFile(u64, ctx.db_file, 0);
        ctx.last_id = try readIntFromFile(u32, ctx.db_file, 8);
        ctx.last_pos = try readIntFromFile(u64, ctx.db_file, 12);
    }

    return ctx;
}

pub fn close(ctx: Ctx) void {
    ctx.db_file.close();
    ctx.ii_file.close();
    ctx.di_file.close();
}

// ===== Hash stuff =====

// ii is short for indirect index
const ii_element_len = 254 + 4;
const ii_count = 256;
const ii_size = ii_element_len * ii_count;

/// The hash function used on the indirect idex file
fn ii_hash_fn(key: []const u8, map_size: u32) u32 {
    var sum: u32 = 0;
    var i: i32 = 0;
    for (key) |c| {
        const signal: i32 = if (@mod(i, 2) == 0) 1 else -1;
        const tmp: i32 = signal * @intCast(i32, c) * 16607;
        sum = @intCast(u32, (if (tmp > 0) tmp else -tmp));
        sum = @mod(sum, @intCast(u32, map_size));
        i += 1;
    }
    const signal: i32 = if (@mod(i, 2) == 0) 1 else -1;
    const tmp: i32 = signal * i * 16607;
    sum = @intCast(u32, (if (tmp > 0) tmp else -tmp));
    sum = @mod(sum, @intCast(u32, map_size));
    return sum;
}

/// Return the id given a secondary key
/// or KeyNotFound error
pub fn readSecKey(ctx: Ctx, key: []const u8) !u32 {
    var data = [_]u8{0} ** ii_element_len;
    try ctx.ii_file.seekTo(ii_hash_fn(key, ii_count) * ii_element_len);
    _ = try ctx.ii_file.readAll(data[0..]);
    const ii_key = data[0..254];
    var id = std.mem.readIntLittle(u32, data[254..258]);

    var has_key = blk: {
        for (key) |k, i| {
            if (k != ii_key[i])
                break :blk false;
        }
        break :blk true;
    };

    if (has_key) {
        return id;
    } else { // Search in reserve position
        try ctx.ii_file.seekTo(ii_size);
        // read until the end
        var j: usize = 0;
        while ((try ctx.ii_file.readAll(data[0..])) > 0) : (j += 1) {
            try ctx.ii_file.seekTo(j * ii_element_len + ii_size);

            //skip empty space
            if (data[0] == 0) continue;

            // check if is the same key
            var hk = blki: {
                for (key) |k, i| {
                    if (k != ii_key[i])
                        break :blki false;
                }
                break :blki true;
            };

            // if found the key
            if (hk) {
                id = std.mem.readIntLittle(u32, data[254..258]);
                return id;
            }
        }
        // Key not found
        return CrudError.KeyNotFound;
    }
}

/// Update the indirect index
fn updateSecKey(ctx: Ctx, key: []const u8, id: u32) !void {
    var ii_buffer = [_]u8{0} ** ii_element_len;
    std.mem.copy(u8, ii_buffer[0..254], key);
    std.mem.copy(u8, ii_buffer[254..258], toBytes(id)[0..]);

    const ii_pos = ii_hash_fn(key, ii_count) * ii_element_len;

    // Read current key
    var data = [_]u8{0} ** 258;
    try ctx.ii_file.seekTo(ii_pos);
    const bytes_read = try ctx.ii_file.readAll(data[0..]);

    //@breakpoint();

    const ii_key = data[0..254];

    var pos_taken = blk: {
        if (bytes_read == 0) {
            break :blk false;
        }
        for (key) |k, i| {
            if (k != ii_key[i])
                break :blk true;
        }
        break :blk false;
    };

    if (pos_taken) {
        try ctx.ii_file.seekTo(ii_size);
        // read until the end
        var i: usize = 0;
        while ((try ctx.ii_file.readAll(data[0..])) > 0) {
            // check for empty space
            if (data[0] == 0) {
                break;
            }
            // incremente header
            try ctx.ii_file.seekBy(ii_element_len);
            i += 1;
        }
        try ctx.ii_file.seekTo(i * ii_element_len + ii_size);
        try ctx.ii_file.writeAll(ii_buffer[0..]);
    } else {
        try ctx.ii_file.seekTo(ii_pos);
        try ctx.ii_file.writeAll(ii_buffer[0..]);
    }
}

/// TODO
/// delete the indirect index
fn deleteSecKey(ctx: Ctx, key: []const u8, id: u32) !void {
    var ii_buffer = [_]u8{0} ** ii_element_len;
    std.mem.copy(u8, ii_buffer[0..254], key);
    std.mem.copy(u8, ii_buffer[254..258], toBytes(id)[0..]);

    const ii_pos = ii_hash_fn(key, ii_count) * ii_element_len;

    // Read current key
    var data = [_]u8{0} ** 258;
    try ctx.ii_file.seekTo(ii_pos);
    const bytes_read = try ctx.ii_file.readAll(data[0..]);

    //@breakpoint();

    const ii_key = data[0..254];

    var pos_taken = blk: {
        if (bytes_read == 0) {
            break :blk false;
        }
        for (key) |k, i| {
            if (k != ii_key[i])
                break :blk true;
        }
        break :blk false;
    };

    if (pos_taken) {
        try ctx.ii_file.seekTo(ii_size);
        // read until the end
        var i: usize = 0;
        while ((try ctx.ii_file.readAll(data[0..])) > 0) {
            // check for empty space
            if (data[0] == 0) {
                break;
            }
            // incremente header
            try ctx.ii_file.seekBy(ii_element_len);
            i += 1;
        }
        try ctx.ii_file.seekTo(i * ii_element_len + ii_size);
        try ctx.ii_file.writeAll(ii_buffer[0..]);
    } else {
        try ctx.ii_file.seekTo(ii_pos);
        try ctx.ii_file.writeAll(ii_buffer[0..]);
    }
}

/// Insert a sugestion to the suggestion list
fn insertSuggestionList(ctx: Ctx, user_id: u32, id: u32) !void {
    try ctx.ii_file.seekTo(0);

    const end_pos = try ctx.ii_file.getEndPos();

    // Find a empty spot
    while (true) {
        // Check for end of file
        const pos = try ctx.ii_file.getPos();
        if (pos == end_pos) break;

        // Read the current id
        var buf = [_]u8{0} ** 4;
        _ = try ctx.ii_file.readAll(buf[0..]);
        const tmp_id = std.mem.readIntLittle(u32, buf[0..]);

        // check if the id empty
        if (tmp_id == 0) {
            try ctx.ii_file.seekBy(-4);
            break;
        }
        try ctx.ii_file.seekBy(4);
    }

    try ctx.ii_file.writeAll(toBytes(id)[0..]);
    try ctx.ii_file.writeAll(toBytes(user_id)[0..]);
}

pub fn deleteSuggestionList(ctx: Ctx, id: u32) !void {
    try ctx.ii_file.seekTo(0);

    const end_pos = try ctx.ii_file.getEndPos();

    // Find the id
    while (true) {
        // Check for end of file
        const pos = try ctx.ii_file.getPos();
        if (pos == end_pos) break;

        // Read the current id
        var buf = [_]u8{0} ** 4;
        _ = try ctx.ii_file.readAll(buf[0..]);
        const current_id = std.mem.readIntLittle(u32, buf[0..]);

        if (current_id == id) {
            try writeIntToFile(u32, ctx.ii_file, 0, pos);
            try writeIntToFile(u32, ctx.ii_file, 0, pos + 4);
            break;
        }
        try ctx.ii_file.seekBy(4);
    }
}

pub fn listSuggestions(al: *Allocator, ctx: Ctx, user_id: u32) ![]u32 {
    try ctx.ii_file.seekTo(0);

    const end_pos = try ctx.ii_file.getEndPos();

    var tmp_list = [_]u32{0} ** 4096;
    var id_count: u32 = 0;

    // Find a empty spot
    while (true) {
        // Check for end of file
        const pos = try ctx.ii_file.getPos();
        if (pos == end_pos) break;

        // Read the current id
        var buf = [_]u8{0} ** 4;
        _ = try ctx.ii_file.readAll(buf[0..]);
        const id = std.mem.readIntLittle(u32, buf[0..]);

        // read user id
        _ = try ctx.ii_file.readAll(buf[0..]);
        const u_id = std.mem.readIntLittle(u32, buf[0..]);

        if (u_id == user_id and id_count < 4096) {
            tmp_list[id_count] = id;
            id_count += 1;
        }
    }

    var result = try al.alloc(u32, id_count);
    std.mem.copy(u32, result, tmp_list[0..id_count]);
    return result;
}

// ======================

/// Allocated memory is freed inside the function
pub fn create(
    al: *Allocator,
    ctx: *Ctx,
    data: []u8,
    sec_key: []const u8,
) !void {
    // update last id
    ctx.last_id = (try readIntFromFile(u32, ctx.db_file, 8)) + 1;
    try writeIntToFile(u32, ctx.db_file, ctx.last_id, 8);

    // write memory block length
    ctx.last_pos = try readIntFromFile(u64, ctx.db_file, 12);
    try writeIntToFile(u32, ctx.db_file, @intCast(u32, data.len) + 4, ctx.last_pos);

    // Find a free spot or use the last position
    const pos: u64 = blk: {
        var _pos: u64 = 0x14;
        while (_pos < ctx.last_pos) {
            const len = try readIntFromFile(u32, ctx.db_file, _pos + 4);
            const block_size = try readIntFromFile(u32, ctx.db_file, _pos);
            if (len == 0 and block_size >= data.len) break :blk (_pos);
            _pos += (len + 4);
        }

        // Update last pos
        ctx.last_pos += (data.len + 8);
        try writeIntToFile(u64, ctx.db_file, ctx.last_pos + data.len + 8, 12);
        break :blk (ctx.last_pos - data.len - 8);
    };

    // write data length
    try writeIntToFile(u32, ctx.db_file, @intCast(u32, data.len), pos + 4);

    // write the data
    try ctx.db_file.seekTo(pos + 8);
    try ctx.db_file.writeAll(data);
    try writeIntToFile(u32, ctx.db_file, ctx.last_id, pos + 8);

    // Update direct index
    try writeIntToFile(u64, ctx.di_file, pos + 4, ctx.last_id * 8);

    // Update indirect index
    try updateSecKey(ctx.*, sec_key, ctx.last_id);
}

/// Allocated memory is freed inside the function
pub fn createSuggestion(
    al: *Allocator,
    ctx: *Ctx,
    data: []u8,
    user_id: u32,
) !void {
    // update last id
    ctx.last_id = (try readIntFromFile(u32, ctx.db_file, 8)) + 1;
    try writeIntToFile(u32, ctx.db_file, ctx.last_id, 8);

    // write memory block length
    ctx.last_pos = try readIntFromFile(u64, ctx.db_file, 12);
    try writeIntToFile(u32, ctx.db_file, @intCast(u32, data.len) + 4, ctx.last_pos);

    // Find a free spot or use the last position
    const pos: u64 = blk: {
        var _pos: u64 = 0x14;
        while (_pos < ctx.last_pos) {
            const len = try readIntFromFile(u32, ctx.db_file, _pos + 4);
            const block_size = try readIntFromFile(u32, ctx.db_file, _pos);
            if (len == 0 and block_size >= data.len) break :blk (_pos);
            _pos += (len + 4);
        }

        // Update last pos
        ctx.last_pos += (data.len + 8);
        try writeIntToFile(u64, ctx.db_file, ctx.last_pos + data.len + 8, 12);
        break :blk (ctx.last_pos - data.len - 8);
    };

    // write data length
    try writeIntToFile(u32, ctx.db_file, @intCast(u32, data.len), pos + 4);

    // write the data
    try ctx.db_file.seekTo(pos + 8);
    try ctx.db_file.writeAll(data);
    try writeIntToFile(u32, ctx.db_file, ctx.last_id, pos + 8);

    // Update direct index
    try writeIntToFile(u64, ctx.di_file, pos + 4, ctx.last_id * 8);

    // Update indirect index
    try insertSuggestionList(ctx.*, user_id, ctx.last_id);
}

/// Read a entity given a id
pub fn read(
    comptime T: type,
    al: *Allocator,
    ctx: Ctx,
    id: u32,
) !T {
    // get position from index
    const pos = try readIntFromFile(u64, ctx.di_file, id * @sizeOf(u64));
    if (pos == 0) {
        return CrudError.InvalidIndex;
    }

    // get data length
    const length = try readIntFromFile(u32, ctx.db_file, pos);

    //@breakpoint();
    // Read data
    var data = try al.alloc(u8, length);
    defer al.free(data);
    try ctx.db_file.seekTo(pos + 4);
    _ = try ctx.db_file.readAll(data[0..]);

    // Create entity
    const result = T.fromByteArray(al, data);
    return result;
}

/// Update a entity given a id
pub fn update(
    al: *Allocator,
    ctx: *Ctx,
    user: var,
) !void {
    comptime if (@TypeOf(user) != Entity and @TypeOf(user) != Suggestion and @TypeOf(user) != Group and @TypeOf(user) != Invite) {
        @compileError("Invalid type for user");
    };

    const user_pos = try readIntFromFile(u64, ctx.di_file, user.id * @sizeOf(u64));
    if (user_pos == 0) {
        return CrudError.InvalidIndex;
    }
    const user_len = try readIntFromFile(u32, ctx.db_file, user_pos);

    const data = try user.toByteArray(al);
    defer al.free(data);

    if (user.bytesSize() > user_len) {
        // set position to trash
        try writeIntToFile(u32, ctx.db_file, 0, user_pos);

        // write memory block length
        ctx.last_pos = try readIntFromFile(u64, ctx.db_file, 12);
        try writeIntToFile(u32, ctx.db_file, @intCast(u32, data.len) + 4, ctx.last_pos);
        // write data length
        try writeIntToFile(u32, ctx.db_file, @intCast(u32, data.len), ctx.last_pos + 4);

        // write the data
        try ctx.db_file.seekTo(ctx.last_pos + 8);
        try ctx.db_file.writeAll(data);

        // Update direct index
        try writeIntToFile(u64, ctx.di_file, ctx.last_pos + 4, user.id * 8);

        // Update last pos
        ctx.last_pos += data.len + 8;
        try writeIntToFile(u64, ctx.db_file, ctx.last_pos, 12);
    } else {
        try ctx.db_file.seekTo(user_pos + 4);
        try ctx.db_file.writeAll(data[0..]);
    }
}

/// Delete a entity given a id
pub fn delete(
    id: u32,
    ctx: Ctx,
) !void {
    const pos = try readIntFromFile(u64, ctx.di_file, id * @sizeOf(u64));
    if (pos == 0) {
        return CrudError.InvalidIndex;
    }
    try writeIntToFile(u64, ctx.di_file, 0, id * @sizeOf(u64));
    try writeIntToFile(u32, ctx.db_file, 0, pos);
}

/// Delete a suggestion given a id
pub fn deleteSuggestion(
    id: u32,
    ctx: Ctx,
) !void {
    const pos = try readIntFromFile(u64, ctx.di_file, id * @sizeOf(u64));
    if (pos == 0) {
        return CrudError.InvalidIndex;
    }
    try writeIntToFile(u64, ctx.di_file, 0, id * @sizeOf(u64));
    try writeIntToFile(u32, ctx.db_file, 0, pos);
    try deleteSuggestionList(ctx, id);
}
