const std = @import("std");
const warn = std.debug.warn;
const toBytes = std.mem.toBytes;
const mem = std.mem;
const Allocator = std.mem.Allocator;

const db = @import("database.zig");

pub const Entity = struct {
    id: u32,
    name: []const u8,
    email: []const u8,
    password: []const u8,

    /// Prints the Entity fiels, debug only
    pub fn printDebug(self: Entity) void {
        warn(
            "\nid: {}\nname: {}\nemail: {}\npassword: {}\n\n",
            .{ self.id, self.name, self.email, self.password },
        );
    }

    pub fn bytesSize(self: Entity) u64 {
        const size = @sizeOf(u32) * 4 + self.name.len +
            self.email.len + self.password.len;
        return size;
    }

    /// Returns a array of bytes
    /// Caller free the memory
    pub fn toByteArray(self: Entity, al: *Allocator) ![]u8 {
        const size = @sizeOf(u32) * 4 + self.name.len +
            self.email.len + self.password.len;
        var index: usize = 0;
        var tmp = try al.alloc(u8, size);
        errdefer al.free(tmp);

        mem.copy(u8, tmp, toBytes(self.id)[0..]);
        index += @sizeOf(u32);

        const fields = [_][]const u8{
            self.name,
            self.email,
            self.password,
        };

        var i: usize = 0;
        while (index < size) : (i += 1) {
            mem.copy(u8, tmp[index..], toBytes(@intCast(u32, fields[i].len))[0..]);
            index += @sizeOf(u32);
            mem.copy(u8, tmp[index..], fields[i][0..(fields[i].len)]);
            index += fields[i].len;
        }

        return tmp;
    }

    /// creates a Entity from a byte array
    /// does not free the array
    pub fn fromByteArray(al: *Allocator, array: []u8) !Entity {
        var result: Entity = undefined;
        var index: usize = 0;

        // Read id
        result.id = std.mem.readIntSliceLittle(u32, array[index..4]);
        index += @sizeOf(u32);

        // Internal function to read a string
        // @i is the initial possition of the string length
        var readStr = struct {
            fn _f(_al: *Allocator, _array: []u8, i: *usize) ![]u8 {
                const length = std.mem.readIntSliceLittle(u32, _array[i.*..]);
                i.* += @sizeOf(u32);
                var str = try _al.alloc(u8, length);
                errdefer _al.free(str);
                mem.copy(u8, str[0..], _array[i.*..(i.* + length)]);
                i.* += length;
                return str;
            }
        }._f;

        result.name = try readStr(al, array, &index);
        result.email = try readStr(al, array, &index);
        result.password = try readStr(al, array, &index);

        return result;
    }

    /// Free the memory of the strings on the entity.
    /// If a arena allocator was used to allocate the memory,
    /// or the memory is on the stack, then don't call this function
    pub fn free(self: Entity, al: *Allocator) void {
        al.free(self.name);
        al.free(self.email);
        al.free(self.password);
    }
};

pub const Suggestion = struct {
    id: u32,
    user_id: u32,
    price: i32, // Price in cents
    product: []const u8,
    store: []const u8,
    obs: []const u8,

    /// Prints the Entity fiels, debug only
    pub fn printDebug(self: Suggestion) void {
        warn(
            "\nid: {}\nuser id: {}\nprice: {d}\nproduct: {}\nstore: {}\nobs: {}\n\n",
            .{ self.id, self.user_id, self.price, self.product, self.store, self.obs },
        );
    }

    pub fn bytesSize(self: Suggestion) u64 {
        const size = @sizeOf(u32) * 2 + @sizeOf(f32) + 3 * @sizeOf(u32) +
            self.product.len + self.store.len + self.obs.len;
        return size;
    }

    /// Returns a array of bytes
    /// Caller free the memory
    pub fn toByteArray(self: Suggestion, al: *Allocator) ![]u8 {
        const size = self.bytesSize();

        var index: usize = 0;
        var tmp = try al.alloc(u8, size);
        errdefer al.free(tmp);

        mem.copy(u8, tmp[index..], toBytes(self.id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.user_id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.price)[0..]);
        index += @sizeOf(f32);

        const fields = [_][]const u8{
            self.product,
            self.store,
            self.obs,
        };

        var i: usize = 0;
        while (index < size) : (i += 1) {
            mem.copy(u8, tmp[index..], toBytes(@intCast(u32, fields[i].len))[0..]);
            index += @sizeOf(u32);
            mem.copy(u8, tmp[index..], fields[i][0..(fields[i].len)]);
            index += fields[i].len;
        }

        return tmp;
    }

    /// creates a Entity from a byte array
    /// does not free the array
    pub fn fromByteArray(al: *Allocator, array: []u8) !Suggestion {
        var result: Suggestion = undefined;
        var index: usize = 0;

        // Read id
        result.id = std.mem.readIntSliceLittle(u32, array[index..4]);
        index += @sizeOf(u32);
        result.user_id = std.mem.readIntSliceLittle(u32, array[index..(index + 4)]);
        index += @sizeOf(u32);
        result.price = std.mem.readIntSliceLittle(i32, array[index..(index + 4)]);
        index += @sizeOf(f32);

        // Internal function to read a string
        // @i is the initial possition of the string length
        var readStr = struct {
            fn _f(_al: *Allocator, _array: []u8, i: *usize) ![]u8 {
                const length = std.mem.readIntSliceLittle(u32, _array[i.*..]);
                i.* += @sizeOf(u32);
                var str = try _al.alloc(u8, length);
                errdefer _al.free(str);
                mem.copy(u8, str[0..], _array[i.*..(i.* + length)]);
                i.* += length;
                return str;
            }
        }._f;

        result.product = try readStr(al, array, &index);
        result.store = try readStr(al, array, &index);
        result.obs = try readStr(al, array, &index);

        return result;
    }

    /// Return a string with the secondary key
    /// string need to be freed later
    pub fn secondaryKey(self: Suggestion, al: Allocator) ![]u8 {
        const buf = try al.alloc(u8, self.product.len + 5);
        std.fmt.bufPrint(buf, "{}_{}", .{ self.id, self.product });
        return buf;
    }

    /// Free the memory of the strings on the entity.
    /// If a arena allocator was used to allocate the memory,
    /// or the memory is on the stack, then don't call this function
    pub fn free(self: Suggestion, al: *Allocator) void {
        al.free(self.product);
        al.free(self.store);
        al.free(self.obs);
    }
};

pub const Group = struct {
    id: u32,
    user_id: u32,
    draw_date: u64,
    meeting_date: u64,
    price: i32,

    is_sorted: u8,
    is_active: u8,

    name: []const u8,
    meeting_place: []const u8,
    obs: []const u8,

    pub fn bytesSize(self: Group) u64 {
        const size = 2 * @sizeOf(u32) + 2 * @sizeOf(u64) + @sizeOf(i32) + 2 * @sizeOf(u8) +
            3 * @sizeOf(u32) + self.meeting_place.len + self.obs.len + self.name.len;
        return size;
    }

    /// Prints the Entity fiels, debug only
    pub fn printDebug(self: Group) void {
        warn(
            \\id: {}
            \\user id: {}
            \\draw date: {}
            \\meeting date: {}
            \\price: {}
            \\is_sorted: {}
            \\is_active: {}
            \\name: {}
            \\meeting place: {}
            \\obs: {}
            \\"
        ,
            .{
                self.id,
                self.user_id,
                self.draw_date,
                self.meeting_date,
                self.price,
                self.is_sorted,
                self.is_active,
                self.name,
                self.meeting_place,
                self.obs,
            },
        );
    }

    /// Returns a array of bytes
    /// Caller free the memory
    pub fn toByteArray(self: Group, al: *Allocator) ![]u8 {
        const size = self.bytesSize();

        var index: usize = 0;
        var tmp = try al.alloc(u8, size);
        errdefer al.free(tmp);

        mem.copy(u8, tmp[index..], toBytes(self.id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.user_id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.draw_date)[0..]);
        index += @sizeOf(u64);
        mem.copy(u8, tmp[index..], toBytes(self.meeting_date)[0..]);
        index += @sizeOf(u64);
        mem.copy(u8, tmp[index..], toBytes(self.price)[0..]);
        index += @sizeOf(i32);

        tmp[index] = self.is_sorted;
        index += @sizeOf(u8);
        tmp[index] = self.is_active;
        index += @sizeOf(u8);

        const fields = [_][]const u8{
            self.name,
            self.meeting_place,
            self.obs,
        };

        var i: usize = 0;
        while (index < size) : (i += 1) {
            mem.copy(u8, tmp[index..], toBytes(@intCast(u32, fields[i].len))[0..]);
            index += @sizeOf(u32);
            mem.copy(u8, tmp[index..], fields[i][0..(fields[i].len)]);
            index += fields[i].len;
        }

        return tmp;
    }

    /// creates a Entity from a byte array
    /// does not free the array
    pub fn fromByteArray(al: *Allocator, array: []u8) !Group {
        var result: Group = undefined;
        var index: usize = 0;

        // Read id
        result.id = std.mem.readIntSliceLittle(u32, array[index..4]);
        index += @sizeOf(u32);
        result.user_id = std.mem.readIntSliceLittle(u32, array[index..(index + 4)]);
        index += @sizeOf(u32);
        result.draw_date = std.mem.readIntSliceLittle(u64, array[index..(index + 8)]);
        index += @sizeOf(u64);
        result.meeting_date = std.mem.readIntSliceLittle(u64, array[index..(index + 8)]);
        index += @sizeOf(u64);
        result.price = std.mem.readIntSliceLittle(i32, array[index..(index + 4)]);
        index += @sizeOf(i32);

        result.is_sorted = array[index];
        index += @sizeOf(u8);
        result.is_active = array[index];
        index += @sizeOf(u8);

        // Internal function to read a string
        // @i is the initial possition of the string length
        var readStr = struct {
            fn _f(_al: *Allocator, _array: []u8, i: *usize) ![]u8 {
                const length = std.mem.readIntSliceLittle(u32, _array[i.*..]);
                i.* += @sizeOf(u32);
                var str = try _al.alloc(u8, length);
                errdefer _al.free(str);
                mem.copy(u8, str[0..], _array[i.*..(i.* + length)]);
                i.* += length;
                return str;
            }
        }._f;

        result.name = try readStr(al, array, &index);
        result.meeting_place = try readStr(al, array, &index);
        result.obs = try readStr(al, array, &index);

        return result;
    }

    /// Return a string with the secondary key
    /// string need to be freed later
    pub fn secondaryKey(self: Group, al: Allocator) ![]u8 {
        const buf = try al.alloc(u8, self.meeting_place.len + 5);
        std.fmt.bufPrint(buf, "{}_{}", .{ self.id, self.name });
        return buf;
    }

    /// Free the memory of the strings on the entity.
    /// If a arena allocator was used to allocate the memory,
    /// or the memory is on the stack, then don't call this function
    pub fn free(self: Group, al: *Allocator) void {
        al.free(self.name);
        al.free(self.meeting_place);
        al.free(self.obs);
    }
};

pub const Invite = struct {
    id: u32,
    group_id: u32,
    invite_date: u64,
    state: u8,

    email: []const u8,

    /// Prints the Entity fiels, debug only
    pub fn printDebug(self: Invite) void {
        warn(
            \\id: {}
            \\group id: {}
            \\invite date: {}
            \\meeting date: {}
            \\state: {}
            \\email: {}
        ,
            .{
                self.id,
                self.group_id,
                self.invite_date,
                self.state,
                self.email,
            },
        );
    }

    pub fn bytesSize(self: Invite) u64 {
        const size = 3 * @sizeOf(u32) +
            @sizeOf(u64) + @sizeOf(u8) + self.email.len;
        return size;
    }

    /// Returns a array of bytes
    /// Caller free the memory
    pub fn toByteArray(self: Invite, al: *Allocator) ![]u8 {
        const size = self.bytesSize();

        var index: usize = 0;
        var tmp = try al.alloc(u8, size);
        errdefer al.free(tmp);

        mem.copy(u8, tmp[index..], toBytes(self.id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.group_id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.invite_date)[0..]);
        index += @sizeOf(u64);
        tmp[index] = self.state;
        index += @sizeOf(u8);

        mem.copy(u8, tmp[index..], toBytes(@intCast(u32, self.email.len))[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], self.email[0..]);
        index += self.email.len;

        return tmp;
    }

    /// creates a Entity from a byte array
    /// does not free the array
    pub fn fromByteArray(al: *Allocator, array: []u8) !Invite {
        var result: Invite = undefined;
        var index: usize = 0;

        // Read id
        result.id = std.mem.readIntSliceLittle(u32, array[index..4]);
        index += @sizeOf(u32);
        result.group_id = std.mem.readIntSliceLittle(u32, array[index..(index + 4)]);
        index += @sizeOf(u32);
        result.invite_date = std.mem.readIntSliceLittle(u64, array[index..(index + 8)]);
        index += @sizeOf(u64);
        result.state = array[index];
        index += @sizeOf(u8);

        // Internal function to read a string
        // @i is the initial possition of the string length
        var readStr = struct {
            fn _f(_al: *Allocator, _array: []u8, i: *usize) ![]u8 {
                const length = std.mem.readIntSliceLittle(u32, _array[i.*..]);
                i.* += @sizeOf(u32);
                var str = try _al.alloc(u8, length);
                errdefer _al.free(str);
                mem.copy(u8, str[0..], _array[i.*..(i.* + length)]);
                i.* += length;
                return str;
            }
        }._f;

        result.email = try readStr(al, array, &index);

        return result;
    }

    /// Return a string with the secondary key
    /// string need to be freed later
    pub fn secondaryKey(self: Invite, al: Allocator) ![]u8 {
        const buf = try al.alloc(u8, self.meeting_place.len + 5);
        std.fmt.bufPrint(buf, "{}_{}", .{ self.id, self.email });
        return buf;
    }

    /// Free the memory of the strings on the entity.
    /// If a arena allocator was used to allocate the memory,
    /// or the memory is on the stack, then don't call this function
    pub fn free(self: Invite, al: *Allocator) void {
        al.free(self.email);
    }
};

pub const Participation = struct {
    id: u32,
    group_id: u32,
    invite_date: u64,
    state: u8,

    /// Prints the Entity fiels, debug only
    pub fn printDebug(self: Invite) void {
        warn(
            \\id: {}
            \\group id: {}
            \\invite date: {}
            \\meeting date: {}
        ,
            .{
                self.id,
                self.group_id,
                self.invite_date,
                self.state,
                self.email,
            },
        );
    }

    pub fn bytesSize(self: Invite) u64 {
        const size = 2 * @sizeOf(u32) + @sizeOf(u64) + @sizeOf(u8) +
            @sizeOf(u32) + self.email.len;
        return size;
    }

    /// Returns a array of bytes
    /// Caller free the memory
    pub fn toByteArray(self: Invite, al: *Allocator) ![]u8 {
        const size = self.bytesSize();

        var index: usize = 0;
        var tmp = try al.alloc(u8, size);
        errdefer al.free(tmp);

        mem.copy(u8, tmp[index..], toBytes(self.id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.group_id)[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], toBytes(self.invite_date)[0..]);
        index += @sizeOf(u64);
        tmp[index] = self.state;
        index += @sizeOf(u8);

        mem.copy(u8, tmp[index..], toBytes(@intCast(u32, self.email.len))[0..]);
        index += @sizeOf(u32);
        mem.copy(u8, tmp[index..], fields[i][0..(self.email.len)]);
        index += fields[i].len;

        return tmp;
    }

    /// creates a Entity from a byte array
    /// does not free the array
    pub fn fromByteArray(al: *Allocator, array: []u8) !Invite {
        var result: Invite = undefined;
        var index: usize = 0;

        // Read id
        result.id = std.mem.readIntSliceLittle(u32, array[index..4]);
        index += @sizeOf(u32);
        result.group_id = std.mem.readIntSliceLittle(u32, array[index..(index + 4)]);
        index += @sizeOf(u32);
        result.invite_date = std.mem.readIntSliceLittle(u64, array[index..(index + 8)]);
        index += @sizeOf(u64);
        result.state = array[index];
        index += @sizeOf(u8);

        // Internal function to read a string
        // @i is the initial possition of the string length
        var readStr = struct {
            fn _f(_al: *Allocator, _array: []u8, i: *usize) ![]u8 {
                const length = std.mem.readIntSliceLittle(u32, _array[i.*..]);
                i.* += @sizeOf(u32);
                var str = try _al.alloc(u8, length);
                errdefer _al.free(str);
                mem.copy(u8, str[0..], _array[i.*..(i.* + length)]);
                i.* += length;
                return str;
            }
        }._f;

        result.email = try readStr(al, array, &index);

        return result;
    }

    /// Return a string with the secondary key
    /// string need to be freed later
    pub fn secondaryKey(self: Invite, al: Allocator) ![]u8 {
        const buf = try al.alloc(u8, self.meeting_place.len + 5);
        std.fmt.bufPrint(buf, "{}_{}", .{ self.id, self.email });
        return buf;
    }

    /// Free the memory of the strings on the entity.
    /// If a arena allocator was used to allocate the memory,
    /// or the memory is on the stack, then don't call this function
    pub fn free(self: Invite, al: *Allocator) void {
        al.free(self.email);
    }
};
