const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    const target = b.standardTargetOptions(.{});

    const mode = b.standardReleaseOptions();
    const windows = b.option(bool, "windows", "create windows build") orelse false;
    const osx = b.option(bool, "osx", "create MacOSX build") orelse false;

    const exe = b.addExecutable("crud-zig", "src/main.zig");
    exe.setTarget(target);

    if (windows) {
        exe.setTarget(.{
            .cpu_arch = .x86_64,
            .os_tag = .windows,
            .abi = .gnu,
        });
    }

    if (osx) {
        exe.setTarget(.{
            .cpu_arch = .x86_64,
            .os_tag = .macosx,
            .abi = .gnu,
        });
    }
    exe.setBuildMode(mode);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
